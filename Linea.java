/**
 * Un ejemplo que modela un Linea usando POO
 * 
 * @author Ejercicio: (Milton Jesús Vera Contreras - miltonjesusvc@ufps.edu.co)
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 *
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class Linea {

    //COMPLETE las cuatro propiedades de la linea, las coordenadas de sus dos puntos
    float x1, y1, x2, y2;

    public Linea() {
    }

    public Linea(float x1, float y1, float x2, float y2) {
        //COMPLETE
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    /*complete GET/SET*/
    /**
     * Método de acceso a la propiedad X1
     *
     * @return el valor de x1
     */
    public float getX1() {
        //COMPLETE
        return this.x1;
    }//fin getX1

    /**
     * *
     * Método para modificación de x1
     *
     * @param x1 del triángulo
     */
    public void setX1(float x1) {
        //COMPLETE
        this.x1 = x1;
    }//fin setX1
    
    /**
     * Método de acceso a la propiedad Y1
     *
     * @return el valor de y1
     */
    public float getY1() {
        //COMPLETE
        return this.y1;
    }//fin getY1

    /**
     * *
     * Método para modificación de y1
     *
     * @param y1 del triángulo
     */
    public void setY1(float y1) {
        //COMPLETE
        this.y1 = y1;
    }//fin setY1
    
    /**
     * Método de acceso a la propiedad X2
     *
     * @return el valor de x2
     */
    public float getX2() {
        //COMPLETE
        return this.x2;
    }//fin getX2

    /**
     * *
     * Método para modificación de x2
     *
     * @param x2 del triángulo
     */
    public void setX2(float x2) {
        //COMPLETE
        this.x1 = x2;
    }//fin setX2
    
    /**
     * Método de acceso a la propiedad Y2
     *
     * @return el valor de y2
     */
    public float getY2() {
        //COMPLETE
        return this.y2;
    }//fin getY2

    /**
     * *
     * Método para modificación de y2
     *
     * @param y2 del triángulo
     */
    public void setY2(float y2) {
        //COMPLETE
        this.y2 = y2;
    }//fin setY2
    

    public String getUbicacion() {
        String ubicacion = "Error, es imposible";
        //Complete para que la salida se lo esperado por los test...
        if(this.estaEnCuadranteI() && this.estaEnCuadranteII()){
            ubicacion = "I y II";
        } else if(this.estaEnCuadranteI() && this.estaEnCuadranteIII()){
            ubicacion = "I y III";
        } else if(this.estaEnCuadranteI() && this.estaEnCuadranteIV()){
            ubicacion = "I y IV";
        } else if(this.estaEnCuadranteII() && this.estaEnCuadranteIII()){
            ubicacion = "II y III";
        } else if(this.estaEnCuadranteII() && this.estaEnCuadranteIV()){
            ubicacion = "II y IV";
        } else if(this.estaEnCuadranteIII() && this.estaEnCuadranteIV()){
            ubicacion = "III y IV";
        }
        else if(this.estaEnCuadranteI() && this.estaEnCuadranteII() && this.estaEnCuadranteIII()){
            ubicacion = "I y II y III";
        } else if(this.estaEnCuadranteI() && this.estaEnCuadranteII() && this.estaEnCuadranteIV()){
            ubicacion = "I y II y IV";
        } else if(this.estaEnCuadranteI() && this.estaEnCuadranteIII() && this.estaEnCuadranteIV()){
            ubicacion = "I y III y IV";
        } else if(this.estaEnCuadranteII() && this.estaEnCuadranteIII() && this.estaEnCuadranteIV()){
            ubicacion = "II y III y IV";
        }
        return ubicacion;
    }//fin getUbicacion
    
    public int getCuantosCuadrantes() {
        int n = 0;
        if(this.estaEnCuadranteI()){
            n++;
        }
        if(this.estaEnCuadranteII()){
            n++;
        }
        if(this.estaEnCuadranteIII()){
            n++;
        }
        if(this.estaEnCuadranteIV()){
            n++;
        }
        return n;//complete
    }//fin getCuantosCuadrantes
    
    public boolean estaEnCuadranteI() {
        if ( (this.getX1() > 0 && this.getY1() > 0) || (this.getX2() > 0 && this.getY2() > 0) ) return true;
        
        if(getIntersectoY() > 0) {
            return true;
        }
        return false;
        
    }//fin estaEnCuadranteI
    
    public boolean estaEnCuadranteII() {
        if((this.getX1() < 0 && this.getY1() > 0) || (this.getX2() < 0 && this.getY2() > 0)) return true;
        if(this.getIntersectoY() > 0) return true;
        return false;
    }//fin estaEnPrimerCuadranteII
    
    public boolean estaEnCuadranteIII() {
        if((this.getX1() < 0 && this.getY1() < 0) || (this.getX2() < 0 && this.getY2() < 0)) return true;
        if(this.getIntersectoY() < 0) return true;
        return false;
    }//fin estaEnCuadranteIII
    
    public boolean estaEnCuadranteIV() {
        if((this.getX1() > 0 && this.getY1() < 0) || (this.getX2() > 0 && this.getY2() < 0)) return true;
        if(this.getIntersectoY() < 0) return true;
        return false;
    }//fin estaEnCuadranteIV
    
    public float getPendiente() {
        /*complete*/
        float m = (this.getY2() - this.getY1()) / (this.getX2() - this.getX1());
        return m;
    }

    public float getIntersectoY() {
        float y = this.getY1() - this.getPendiente() * getX1();
        return y;/*complete*/
    }
}//fin clase Linea


