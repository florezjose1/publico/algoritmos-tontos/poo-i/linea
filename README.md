### Línea

Determinar la ubicación de una Línea en el Plano Cartesiano.

1. La línea puede estar en un solo cuadrante: I, II, III, IV
2. La línea puede estar en dos cuadrantes: (I y II), (I y III), (I y IV), (II y III), (II y IV), (III y IV)
3. La línea puede estar en tres cuadrantes, (I, II y III), (I, II y IV), (I, III y IV), (II, III y IV)
4. Los casos de línea horizontal o sobre el eje X se consideran de los planos I y/o II según corresponda
5. Los casos de línea vertical o sobre el eje Y se consideran de los planos I y/o IV según corresponda

- Solo requiere la clase Línea

Versión Bluej: `4.2.2`

Versión Java: `11.0.6`

Hecho con ♥ por [Jose Florez](https://joseflorez.co)
