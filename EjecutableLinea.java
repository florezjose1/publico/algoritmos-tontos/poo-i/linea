
/**
 * Main / ejecutable para realizar calculos con Linea.
 * @author (Milton Jesús Vera Contreras - miltonjesusvc@ufps.edu.co)
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class EjecutableLinea {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Linea l = new Linea();
       FrmLinea gui = new FrmLinea(l);
       gui.setVisible(true);
    }

}